use std::error::Error;

use image::{io::Reader as ImageReader, GenericImageView};
use reqwest::Client;
use serde_json::Value;

const BASE_URL: &'static str = "http://place.dev.paycomhq.com/socket.io/?EIO=4&transport=polling";

fn print_usage(program: &str, opts: getopts::Options) {
    let brief = format!("Usage: {} FILE [options]", program);
    print!("{}", opts.usage(&brief));
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = std::env::args().collect();
    let program = args[0].clone();

    let mut opts = getopts::Options::new();
    opts.optopt("x", "", "Canvas column to place image (default: 0)", "X");
    opts.optopt("y", "", "Canvas row to place image(default: 0)", "Y");
    opts.optopt("i", "clientid", "Client identifier string", "ID");
    opts.optflag("h", "help", "Print this help menu");
    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => {
            panic!("{}", f.to_string())
        }
    };

    if matches.opt_present("h") {
        print_usage(&program, opts);
        return Ok(());
    }

    let x = matches.opt_str("x").map(|v| v.parse::<usize>().unwrap()).unwrap_or(0);
    let y = matches.opt_str("y").map(|v| v.parse::<usize>().unwrap()).unwrap_or(0);
    let id = matches.opt_str("i").unwrap_or("abcd".into());

    let file = if !matches.free.is_empty() {
        matches.free[0].clone()
    } else {
        print_usage(&program, opts);
        return Ok(());
    };

    paint_image(file, id, Point::new(x, y)).await?;

    Ok(())
}

async fn paint_image(file: String, client_id: String, offset: Point) -> Result<(), Box<dyn Error>> {
    let mut client = PlaceClient::new(client_id);

    client.connect().await?;
    client.introduce().await?;
    client.place_image(file, offset).await?;

    Ok(())
}

struct PlaceClient {
    client: Client,
    url: String,
    sid: String,
    client_id: String,
}

impl PlaceClient {
    fn new(client_id: String) -> Self {
        Self {
            client: Client::new(),
            url: "".into(),
            sid: "".into(),
            client_id,
        }
    }

    async fn connect(&mut self) -> Result<String, Box<dyn Error>> {
        let res = self.client.get(BASE_URL).send().await?; // @@@ Was this supposed to be GET or POST?

        let body_text = res.text().await?;

        let body: Value = serde_json::from_str(&body_text)?;
        self.sid = body["sid"].as_str().unwrap().into();
        self.url = format!("{}&sid={}", BASE_URL, self.sid);

        tokio::spawn(self.client.post(&self.url).body("40").send());

        // @@@ Start a tokio interval to poll() and pong the pings

        Ok(self.sid.clone())
    }

    async fn introduce(&self) -> Result<(), Box<dyn Error>> {
        let res = self
            .client
            .post(&self.url)
            .body(format!(r#"42["introduce","{}"]"#, self.client_id))
            .send()
            .await?;

        let _body = res.text().await?;
        // @@@ Parse the current board (assuming we care about it)

        Ok(())
    }

    async fn place_image(&self, file: String, offset: Point) -> Result<(), Box<dyn Error>> {
        let img = ImageReader::open(file)?.decode()?;

        for (x, y, _color) in img.pixels().skip(0) {
            if _color.0[3] == 0 {
                // Skip transparent pixels
                continue;
            }

            let p = offset.add(x as usize, y as usize);
            let hex_color = format!("#{:X}{:X}{:X}", _color.0[0], _color.0[1], _color.0[2]);
            self.pixel(p, hex_color).await?;

            tokio::time::sleep(tokio::time::Duration::from_millis(100)).await;
        }

        Ok(())
    }

    async fn pixel(&self, p: Point, color: String) -> Result<(), Box<dyn Error>> {
        println!("Placing {} at {},{}", color, p.x, p.y);
        self.client
            .post(&self.url)
            .body(format!(
                r#"42["pixel",{{"x":{},"y":{},"color":"{}"}}]"#,
                p.x, p.y, color
            ))
            .send()
            .await?;

        Ok(())
    }

    async fn poll(&self) -> Result<(), Box<dyn Error>> {
        let res = self.client.get(&self.url).send().await?;
        let _body = res.text().await?;
        // @@@ Parse message type (we might need to handle PING (2)). We might consider making an Enum
        // for the message response

        Ok(())
    }
}

struct Point {
    x: usize,
    y: usize,
}

impl Point {
    fn new(x: usize, y: usize) -> Self {
        Point { x, y }
    }

    fn add(&self, x: usize, y: usize) -> Self {
        Point {
            x: self.x + x,
            y: self.y + y,
        }
    }
}
